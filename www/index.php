﻿<?php
/*
 * Сделать форму с 4 мя инпутами - имя, email, фамилия и пол. Сделать сабмит этой формы аяксом.
 * В php скрипте обработчике принимать данные из этой формы. Инициализировать там массив с юзерами (как будто бы его получили из базы данных).
 * Если пользователь с данным email в этом массиве, то на странице с формой вывести alert('User already exists') иначе alert(‘Ok’).
 * Создать репозиторий git на gitlab, запушить туда всё что вы сделали по этому пункту, чтобы Илья посмотрел.
 * */
?>
<html>
<head>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/mail.js"></script>
</head>
<body>
<div class="row">
    <div class="col-md-2 col-md-offset-5">
        <h3>Register</h3>
        <form class="form-horizontal" id="send">
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control" id="name" placeholder="Name" required>
            </div>
            <div class="form-group">
                <label for="lastName">Last name</label>
                <input class="form-control" id="lastName" placeholder="Last name" required>
            </div>
            <div class="form-group">
                <label class="radio-inline">
                    <input class="radio" type="radio" name="sex" id="male" value="m" checked> Male
                </label>
                <label class="radio-inline">
                    <input class="radio" type="radio" name="sex" id="female" value="f"> Female
                </label>
            </div>
            <div class="form-group">
                <label for="email">Email address</label>
                <input class="form-control" id="email" placeholder="Email" required>
            </div>
            <button id="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
</body>
</html>