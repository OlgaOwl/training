$(document).ready(function () {

    $("#submit").click(function (e) {
        e.preventDefault(); //  позволяет предотвратить выполнение стандартного действия, например такого как переход на сайт после щелчка по ссылке или отправка формы после щелчка на кнопку отправления (то есть input type=submit).
        var name = $("#name").val();
        var lastName = $("#lastName").val();
        var sex = $("input:checked").val();
        var email = $("#email").val();

        if(name == '' || lastName == '' || sex == '' || email == ''){
            alert('Incorrect form data');
            return;
        }

        $.ajax({
            type: "POST",
            url: "includes/mail.php",
            data: {
                "name": name,
                "lastName": lastName,
                "sex": sex,
                "email": email
            },
            success: function(result){
                alert(result);
            }
        });
        return false;
    });
});
